import { BOARD_HEIGHT, BOARD_WIDTH } from './constants'

/**
 * Calculation of the load of blocks on the platform
 * @param {array} blocks - All blocks on one side
 * @param {string} side - Platform side
 * @returns {number}
 */
const calculateMomentum = (blocks, side) => {
  let momentum = 0

  blocks.forEach(block => {
    const position = side === 'left'
      ? (BOARD_WIDTH / 2 - block.horizontalPosition)
      : (block.horizontalPosition - BOARD_WIDTH / 2)

    momentum += block.weight * (position / BOARD_WIDTH)
  })

  return momentum
}

/**
 * Calculate sides difference and rotation side
 * @param {array} rightBlocks - All blocks on the right side
 * @param {array} leftBlocks - All blocks on the left side
 * @returns {{sidesDifference: number, angleRotation: number}}
 */
export const platformAngle = (rightBlocks, leftBlocks) => {
  const rightMomentum = calculateMomentum(rightBlocks, 'right')
  const leftMomentum = calculateMomentum(leftBlocks, 'left') || 0

  const sidesDifference = Math.abs(leftMomentum - rightMomentum)
  const angleRotation = leftMomentum > rightMomentum ? -1 : 1

  return { sidesDifference, angleRotation }
}

/**
 * Calculate weight difference between platform sides
 * @param {array} rightBlocks - All blocks on the right side
 * @param {array} leftBlocks - All blocks on the left side
 * @returns {number}
 */
export const sidesWeightDifference = (rightBlocks, leftBlocks) => {
  let rightBlocksWeight = 0
  let leftBlocksWeight = 0

  rightBlocks.forEach((block) => {
    rightBlocksWeight += block.weight
  })

  leftBlocks.forEach((block) => {
    leftBlocksWeight += block.weight
  })

  return Math.abs(leftBlocksWeight - rightBlocksWeight)
}

/**
 * Calculate that block is on the board
 * @param {object} fallingBlock - Block which currently falling down
 * @param {number} platformAngle - Angle of the platform
 * @returns {boolean}
 */
export const blockOnBoard = (fallingBlock, platformAngle) => {
  const board = document.querySelector('.balance--board').getBoundingClientRect()
  const navigation = document.querySelector('.navigation').getBoundingClientRect()
  const block = document.querySelector(`#block-${fallingBlock.id}`).getBoundingClientRect()

  const angle = board.bottom - board.top - BOARD_HEIGHT
  const burden = (fallingBlock.horizontalPosition * angle) / BOARD_WIDTH

  const platformRotation = platformAngle >= 0
    ? board.top + burden - block.height - navigation.height
    : board.bottom - BOARD_HEIGHT - burden - block.height - navigation.height

  return fallingBlock.verticalPosition >= platformRotation
}
