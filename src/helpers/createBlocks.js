import { v4 } from 'uuid'
import {
  MIN_WEIGHT,
  MAX_WEIGHT,
  MAX_RGB_VALUE,
  BOARD_WIDTH,
  BLOCK_SIZE_DIFFERENCE
} from './constants'

class BlockConstructor {
  /**
   * Create block
   * @constructor
   * @param options
   */
  constructor (options) {
    this.id = v4()
    this.side = options.blockSide
    this.type = this.getRandomNumber(1, 3)
    this.weight = this.getRandomNumber(MIN_WEIGHT, MAX_WEIGHT)
    this.size = 1 + BLOCK_SIZE_DIFFERENCE * this.weight
    this.color = this.blockColor

    if (this.side === 'left') {
      this.verticalPosition = 0
      this.horizontalPosition = this.getRandomNumber(0, BOARD_WIDTH / 2 - 1)
    } else {
      this.verticalPosition = null
      this.horizontalPosition = this.getRandomNumber(BOARD_WIDTH / 2 + 1, BOARD_WIDTH)
    }
  }

  /**
   * RGB color for block
   * @returns {string}
   */
  get blockColor () {
    const r = this.getRandomNumber(0, MAX_RGB_VALUE)
    const g = this.getRandomNumber(0, MAX_RGB_VALUE)
    const b = this.getRandomNumber(0, MAX_RGB_VALUE)

    return `rgb(${r}, ${g}, ${b})`
  }

  /**
   * Random number between the minimum and maximum values
   * @param {number} min - Minimal value
   * @param {number} max - Maximal value
   * @returns {number}
   */
  getRandomNumber (min, max) {
    return Math.floor(Math.random() * (max - min) + min)
  }

  /**
   * Move block position to left
   * @returns {number}
   */
  moveLeft () {
    return this.horizontalPosition - 1
  }

  /**
   * Move block position to right
   * @returns {number}
   */
  moveRight () {
    return this.horizontalPosition + 1
  }

  /**
   * Move block position down
   * @returns {number}
   */
  falling () {
    return this.verticalPosition + 1
  }
}

/**
 * Create blocks on the left and right sides
 * @returns {{}}
 */
export const createBlocks = () => {
  const blocks = {}

  for (let i = 0; i < 2; i++) {
    const blockSide = i % 2 === 0 ? 'right' : 'left'

    const figure = new BlockConstructor({ blockSide })
    blocks[figure.id] = figure
  }

  return blocks
}
