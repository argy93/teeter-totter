import Vue from 'vue'
import Vuex from 'vuex'
import gameControl from './modules/gameControl'
import block from './modules/block'
import game from './modules/game'

Vue.use(Vuex)

export default new Vuex.Store({
  actions: gameControl,
  modules: {
    game,
    block
  },
  strict: process.env.NODE_ENV !== 'production'
})
