import {
  sidesWeightDifference,
  platformAngle
} from '@/helpers/utils'
import {
  BLOCK_SIZE_DIFFERENCE,
  MAX_PLATFORM_ANGLE,
  MIN_PLATFORM_ANGLE,
  SIDES_DIFFERENCE
} from '@/helpers/constants'

export default {
  platformMovement ({ state, dispatch, rootState }) {
    setTimeout(() => {
      if (state.status !== '') {
        const { rightBlocks, leftBlocks } = rootState.block
        const {
          sidesDifference,
          angleRotation
        } = platformAngle(rightBlocks, leftBlocks)
        const newAngle = state.platformAngle + sidesDifference * BLOCK_SIZE_DIFFERENCE * angleRotation

        dispatch('platformStatus', newAngle)
      }
    }, 200)
  },
  platformStatus ({ state, commit, dispatch, rootState }, newAngle) {
    const { rightBlocks, leftBlocks } = rootState.block
    const weightDifference = sidesWeightDifference(rightBlocks, leftBlocks)

    if (
      weightDifference >= SIDES_DIFFERENCE ||
      newAngle < MIN_PLATFORM_ANGLE ||
      newAngle > MAX_PLATFORM_ANGLE
    ) {
      commit('setPlatformAngle', 0)
      dispatch('restartGame', null, { root: true })
      return
    }

    commit('setPlatformAngle', newAngle)
    commit('setScore', state.score + 1)

    if (state.status !== 'pause') {
      dispatch('platformMovement')
    }
  }
}
