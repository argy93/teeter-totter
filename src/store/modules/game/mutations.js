import initialState from './state'

export default {
  setStatus (state, payload) {
    state.status = payload
  },
  setScore (state, payload) {
    state.score = payload
  },
  resetState (state) {
    const newState = initialState()
    Object.keys(newState).forEach(key => {
      state[key] = newState[key]
    })
  },
  setPlatformAngle (state, payload) {
    state.platformAngle = payload
  }
}
