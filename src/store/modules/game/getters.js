export default {
  getStatus (state) {
    return state.status
  },
  getScore (state) {
    return state.score
  },
  getPlatformAngle (state) {
    return state.platformAngle
  }
}
