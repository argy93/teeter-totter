const initialState = () => ({
  status: '',
  score: 0,
  platformAngle: 0
})

export default initialState
