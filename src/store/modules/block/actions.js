import { createBlocks } from '@/helpers/createBlocks'
import { blockOnBoard } from '@/helpers/utils'

export default {
  updateBlocks ({ commit, dispatch }) {
    const blocks = createBlocks()

    Object.values(blocks).forEach(block => {
      if (block.verticalPosition === null) {
        commit('updateRightBlocks', block)
      } else {
        commit('updateFallingBlock', block)
      }
    })

    dispatch('startFalling')
  },
  startFalling ({ state, commit, dispatch, rootState }) {
    const { status, platformAngle } = rootState.game

    setTimeout(() => {
      if (status !== '' && state.fallingBlock) {
        if (blockOnBoard(state.fallingBlock, platformAngle)) {
          commit('updateFallingBlockPosition', null)
          commit('updateLeftBlocks', state.fallingBlock)
          commit('updateFallingBlock', null)
          commit('speedIncrease')

          dispatch('game/platformMovement', null, { root: true })
          dispatch('updateBlocks')
        } else {
          if (status !== 'pause') {
            commit('oneFallingStep')
            dispatch('startFalling')
          }
        }
      }
    }, state.currentSpeed)
  }
}
