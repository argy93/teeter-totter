import initialState from './state'
import { MIN_FALLING_SPEED } from '@/helpers/constants'

export default {
  resetState (state) {
    const newState = initialState()
    Object.keys(newState).forEach(key => {
      state[key] = newState[key]
    })
  },
  speedIncrease (state) {
    if (state.currentSpeed > MIN_FALLING_SPEED) {
      state.currentSpeed--
    }
  },
  updateLeftBlocks (state, payload) {
    state.leftBlocks.push(payload)
  },
  updateRightBlocks (state, payload) {
    state.rightBlocks.push(payload)
  },
  updateFallingBlock (state, payload) {
    state.fallingBlock = payload
  },
  updateFallingBlockPosition (state, payload) {
    state.fallingBlock.verticalPosition = payload
  },
  oneFallingStep (state) {
    state.fallingBlock.verticalPosition = state.fallingBlock.falling()

    document
      .getElementById(`block-${state.fallingBlock.id}`)
      .style
      .top = `${state.fallingBlock.verticalPosition}px`
  },
  moveFallingBlock (state, functionName) {
    state.fallingBlock.horizontalPosition = state.fallingBlock[functionName]()
  }
}
