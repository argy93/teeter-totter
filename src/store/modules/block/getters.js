export default {
  getLeftBlocks (state) {
    return state.leftBlocks
  },
  getRightBlocks (state) {
    return state.rightBlocks
  },
  getFallingBlock (state) {
    return state.fallingBlock
  }
}
