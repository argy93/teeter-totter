import { MAX_FALLING_SPEED } from '@/helpers/constants'

const initialState = () => ({
  leftBlocks: [],
  rightBlocks: [],
  fallingBlock: null,
  currentSpeed: MAX_FALLING_SPEED
})

export default initialState
