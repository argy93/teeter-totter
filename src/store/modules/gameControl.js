export default {
  startGame ({ commit, dispatch }) {
    commit('game/setStatus', 'start')
    dispatch('block/updateBlocks')
  },
  restartGame ({ commit }) {
    commit('game/setStatus', '')
    commit('game/resetState')
    commit('block/resetState')
  },
  pauseGame ({ commit }) {
    commit('game/setStatus', 'pause')
  },
  continueGame ({ state, commit, dispatch }) {
    commit('game/setStatus', 'continue')
    dispatch('block/startFalling')

    if (state.block.leftBlocks.length) {
      dispatch('game/platformMovement')
    }
  }
}
